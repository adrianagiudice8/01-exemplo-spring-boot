package br.com.itau.marketplace.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.marketplace.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
