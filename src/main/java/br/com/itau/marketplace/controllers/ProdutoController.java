package br.com.itau.marketplace.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.marketplace.models.MensagemErro;
import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.services.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
	@Autowired
	ProdutoService produtoService;

	@GetMapping
	public Iterable<Produto> listarProdutos() {
		return produtoService.buscarProdutos();
	}
	
	@GetMapping("/{id}")
	public Produto listarProdutoPorId(@PathVariable int id) {
		return produtoService.buscarProdutoPorId(id);
	}
	
	@PostMapping
	public ResponseEntity<?> inserirProduto(@RequestBody Produto produto) {
		boolean deuCerto = produtoService.inserir(produto);
		
		if(! deuCerto) {
			MensagemErro erro = new MensagemErro("Usuário não encontrado");
			return ResponseEntity.badRequest().body(erro);
		}
	
		return ResponseEntity.status(201).build();
	}
}
